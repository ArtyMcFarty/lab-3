1. Настроить сборку js в gulp. Настроить es-lintner. В Gulpfile создана задача Lint, я не включал ее в задачу сборки, запускаю отдельно в терминале.

2. Реализовать hamburger меню в мобильной версии сайта. Файл common.js.

3. Слайдер с анимацией fadeIn/fadeOut. Файл slider.js.

4. pop-up форма обратной связи. Файл popup.js.

5. Валидация формы. Файл validform.js.

6. Используя XMLHttpRequest, считывать текстовые данные из json на страницу. Реализовать промис-обертку вокруг XMLHttpRequest. Файл gettext.js и text.json.